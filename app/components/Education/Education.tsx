"use client"
import React from 'react'
import logo from '../../Assets/logo1.png'
import Image from 'next/image'
import { Slide } from 'react-awesome-reveal'

const Education = () => {
  return (
    <Slide triggerOnce={true}>
      <div>
        <p className='text-6xl text-white mx-8 md:mx-24 my-6 md:my-12'>Education</p>
        <div className='md:flex mx-8 md:mx-24'>
          <div className='md:w-1/6'>
            <Image src={logo} alt='' />
          </div>
          <div className='md:w-5/6 text-white'>
            <div>
              <p className='text-4xl'>Madan Mohan Malviya University of Technology Gorakhpur</p>
              <br />
              <p className='text-3xl leading-10'>Bachelor of Technology in Mechanical Engineering</p>
              <p className='text-xl'>August 2019 - July 2023</p>
            </div>
          </div>
        </div>
        <hr className="w-11/12 h-1 mx-auto my-4 bg-gray-100 border-0 rounded md:my-10 dark:bg-gray-700" />
      </div>
    </Slide>
  )
}

export default Education
