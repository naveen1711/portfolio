"use client"
import React from 'react'
import gif from '../../Assets/giphy.gif'
import Image from 'next/image'
import { Slide } from 'react-awesome-reveal'

const FrontPage2 = () => {
  return (
    <div className='md:flex md:mx-24 text-white md:h-lvh'>
      <div className='md:w-1/2 p-12'>
        <Slide triggerOnce={true} direction='left' duration={2000}>
          <Image src={gif} alt='...' />
        </Slide>
      </div>
      <div className='md:w-1/2'>
        <Slide triggerOnce={true} direction='right' duration={2000}>
          <div className=''>
            <p className='text-6xl font-medium text-center md:text-left md:leading-10'>Let Me Introduce Myself</p>
            <br /><br />
            <p className='text-2xl py-3 text-center md:text-left md:leading-10'>I fell in love with programming and I have atleast learned something.</p>
            <p className='text-2xl py-3 text-center md:text-left md:leading-10'>I am fluent in C, C++, Java and Python. Also have knowledge of HTML, CSS and Javascript.</p>
            <p className='text-2xl py-3 text-center md:text-left md:leading-10'>Currently learning Spring Boot and Microservices. Building UI using Angular.</p>
            <p className='text-2xl py-3 text-center md:text-left md:leading-10'>I am also currently immersed in mastering Data Structure and Algorithms, constantly seeking to enhance my problem-solving skills.</p>
            <p className='text-2xl py-3 text-center md:text-left md:leading-10'>Whenever possible, I also apply my passion for developing products with Node.js and Modern Javascript Library and Frameworks like React.js and Next.js</p>
            <p className='text-2xl py-3 text-center md:text-left'>A die-hard fan of Anime & Manga.</p>
          </div>
        </Slide>
      </div>
    </div >
  )
}

export default FrontPage2
