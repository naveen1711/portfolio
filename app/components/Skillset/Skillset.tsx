"use client"
import React from 'react'
import Link from 'next/link'
import { TbBrandCpp, TbBrandNextjs } from 'react-icons/tb'
import { FaCss3Alt, FaGithub, FaHtml5, FaJava, FaNode, FaPython, FaReact } from 'react-icons/fa'
import { IoLogoJavascript } from 'react-icons/io'
import { BiLogoSpringBoot } from 'react-icons/bi'
import { GrMysql } from 'react-icons/gr'
import { SiMongodb } from 'react-icons/si'
import { Fade } from 'react-awesome-reveal'

const Skillset = () => {
  return (
    <main>
      <p className='text-6xl text-white text-center'>Professional Skillset</p>
      <br /><br />
      <Fade duration={2000} triggerOnce={true}>
        <div className='grid grid-cols-1 md:grid md:grid-cols-4 gap-10 mx-8 md:mx-24'>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <TbBrandCpp className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaJava className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaPython className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaCss3Alt className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaHtml5 className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <IoLogoJavascript className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaNode className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaReact className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <TbBrandNextjs className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <BiLogoSpringBoot className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <GrMysql className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <SiMongodb className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaGithub className='text-8xl text-white mx-auto' />
          </Link>
        </div>
      </Fade>
    </main>
  )
}

export default Skillset
