import Image from "next/image";
import FrontPage1 from "./components/Home/FrontPage1";
import FrontPage2 from "./components/Home/FrontPage2";
import Proficiency from "./components/Proficiency/Proficiency";
import Education from "./components/Education/Education";

export default function Home() {
  return (
    <>
      <FrontPage1 />
      <FrontPage2 />
      <Proficiency />
      <Education />
    </>
  );
}
