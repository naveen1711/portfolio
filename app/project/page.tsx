import React from 'react'
import ProjectCard from '../components/ProjectCard'
import gif from '../Assets/giphy.gif'

const page = () => {
  return (
    <main>
      <p className='text-2xl md:text-6xl text-center text-white md:mt-12 mx-6'>Here are few projects I have worked on recently</p>
      <div className='grid grid-cols-1 md:grid md:grid-cols-3 gap-4 mx-8 md:mx-24 mt-12  md:mt-24'>
        <ProjectCard
          title="iNoteBook"
          // img={gif}
          description="This is a Web Application which solves the problem of storing notes without worrying to loose it. Front-end is build using ReactJs and backend on Express. MongoDb Atlas database is used to store the user credentials and personal notes." github="https://github.com/1711naveen/iNotebook-Frontend" />

        <ProjectCard
          title="Sorting-Visualiser"
          // img={gif}
          description="Used the knowledge of Data Sturcture and Algorithms to implement sorting of array of numbersand visualising it with the help of HTML, CSS and Javascript."
          github="https://github.com/1711naveen/Sorting-Visualiser" />

        <ProjectCard
          title="Sorting-Visualiser"
          // img={gif}
          description="Used the knowledge of Data Sturcture and Algorithms to implement sorting of array of numbersand visualising it with the help of HTML, CSS and Javascript."
          github="https://github.com/1711naveen/Sorting-Visualiser" />

        {/* <ProjectCard /> */}
      </div>
    </main>
  )
}

export default page
