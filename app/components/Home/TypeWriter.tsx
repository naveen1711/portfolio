"use client"
import React from 'react'
import { Typewriter } from 'react-simple-typewriter'

const TypeWriter = () => {
  return (
    <div className='App'>
      <p className='text-white text-4xl md:text-6xl md:mx-0 mx-8'>
        <span className=''>
          <Typewriter
            words={['Software Developer', 'Freelancer', 'and', 'Java Developer']}
            loop={true}
            cursor
            cursorStyle='|'
            typeSpeed={120}
            deleteSpeed={150}
            delaySpeed={1000}
          />
        </span>
      </p>
    </div>

  )
}

export default TypeWriter
