"use client"
import React from 'react'
import gif from '../../Assets/giphy.gif'
import Image from 'next/image'
import TypeWriter from './TypeWriter'
import { FaGithub, FaGitlab, FaLinkedin } from 'react-icons/fa'
import { IoMdMail } from 'react-icons/io'
import Link from 'next/link'
import { Slide } from 'react-awesome-reveal'

const FrontPage1 = () => {
  return (
    <Slide triggerOnce={true} direction='up' duration={2000} >
      <div className='md:flex md:mx-24 text-white h-lvh'>
        <div className='mt-20 md:mt-24 md:w-1/2 '>
          <div className='md:grid grid-rows-4 grid-flow-col gap-8'>
            <div className='text-5xl md:text-7xl text-center md:text-left font-medium'>Hi all, I&apos;m Naveen👋</div>
            <div>
              <TypeWriter />
            </div>
            <div className='flex flex-wrap justify-evenly md:justify-start align-middle text-4xl mt-8 md:mt-4'>
              <div className='rounded-full pe-2'>
                <Link href='/'><FaGithub /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><FaLinkedin /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><IoMdMail /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><FaGitlab /></Link>
              </div>
              <div className='rounded-full px-2 '>
                <Link href='/'><FaGithub /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><FaLinkedin /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><IoMdMail /></Link>
              </div>
              <div className='rounded-full px-2'>
                <Link href='/'><FaGitlab /></Link>
              </div>
            </div>
            <div className='flex justify-center md:justify-start items-center mt-8'>
              <button type="button" className="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-xl px-5 py-2.5 text-center md:me-16 mb-2 ">Contact Me</button>
              <button type="button" className="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-xl px-5 py-2.5 text-center me-2 mb-2">See My Resume</button>
            </div>
          </div>
        </div>
        <div className='mt-12 md:mt-24 md:w-1/2 md:px-24 p-6'>
          <Image src={gif} alt='...' className='' />
        </div>
      </div>
    </Slide>
  )
}

export default FrontPage1
