import Link from 'next/link'
import React from 'react'
import { Fade } from 'react-awesome-reveal'
import { FaLinux, FaWindows } from 'react-icons/fa'
import { SiPostman } from 'react-icons/si'
import { TbBrandVscode } from 'react-icons/tb'

const ToocStack = () => {
  return (
    <main>
      <p className='text-6xl text-white text-center'>Tools I Use</p>
      <br /><br />
      <Fade duration={2000} triggerOnce={true}>
        <div className='grid grid-cols-1 md:grid md:grid-cols-4 gap-4 mx-8 md:mx-24'>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaWindows className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <FaLinux className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <TbBrandVscode className='text-8xl text-white mx-auto' />
          </Link>
          <Link href="/" className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <SiPostman className='text-8xl text-white mx-auto' />
          </Link>
        </div>
      </Fade>
    </main>
  )
}

export default ToocStack
