"use client"
import Image from 'next/image'
import React from 'react'
import gif from '../../Assets/giphy.gif'
import { Slide } from 'react-awesome-reveal'

const Proficiency = () => {
  return (
    <Slide triggerOnce={true}>
      <div className='md:flex mx-8 md:mx-20'>
        <div className='md:w-3/5'>
          <p className='text-6xl text-white'>Proficiency</p>
          <br /><br />
          <div className="mb-1 text-2xl font-medium dark:text-white">Frontend/Design</div>
          <br />
          <div className="w-full h-6 bg-gray-200 rounded-full dark:bg-gray-700">
            <div className="h-6 bg-blue-600 rounded-full dark:bg-blue-300" style={{ width: "85%" }}></div>
          </div>
          <br /><br />
          <div className="mb-1 text-2xl font-medium dark:text-white">Backend</div>
          <br />
          <div className="w-full h-6 bg-gray-200 rounded-full dark:bg-gray-700">
            <div className="h-6 bg-blue-600 rounded-full dark:bg-blue-300" style={{ width: "82%" }}></div>
          </div>
          <br /><br />
          <div className="mb-1 text-2xl font-medium dark:text-white">Programming</div>
          <br />
          <div className="w-full h-6 bg-gray-200 rounded-full dark:bg-gray-700">
            <div className="h-6 bg-blue-600 rounded-full dark:bg-blue-300" style={{ width: "92%" }}></div>
          </div>
          <br /><br />
        </div>
        <div className='md:w-2/5 flex justify-center items-center'>
          <Image src={gif} alt='...' />
        </div>
      </div>
    </Slide>
  )
}

export default Proficiency
