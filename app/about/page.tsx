"use client"
import React from 'react'
import gif from '../Assets/giphy.gif'
import Image from 'next/image'
import Skillset from '../components/Skillset/Skillset'
import ToocStack from '../components/Skillset/ToocStack'
import { Slide } from 'react-awesome-reveal'

const page = () => {
  return (
    <>
      <Slide triggerOnce={true} direction='up' duration={2000} >
        <div className='md:flex ps-8 pe-8 md:mx-24 text-white md:h-lvh'>
          <div className='mt-24 md:w-1/2'>
            <p className='text-6xl md:text-center'>Know Who I&apos;M</p>
            <br /><br />
            <p className='text-2xl leading-loose'>Hello Everyone, I am Naveen Yadav from Kanpur UttarPradesh, India. I have completed B.Tech in Mechanical Engineering from Madan Mohan Malaviya University of Technology, Gorakhpur. Interested in Machine Learning & Artificial Intelligence.</p>
            <br /><br />
            <p className='text-2xl'>
              Apart from coding, some other activities that I love to do!
            </p>
            <br />
            <ul className='text-2xl list-disc leading-relaxed'>
              <li>Playing Chess & Cricket</li>
              <li>Reading Books</li>
              <li>Watching Anime</li>
            </ul>
          </div>
          <div className='mt-24 md:w-1/2 '>
            <Image src={gif} alt='' className='md:my-24 md:ms-24 ps-4 pe-4' />
          </div>
        </div >
      </Slide>
      <Skillset />
      <br /><br />
      <ToocStack />
    </>
  )
}

export default page
